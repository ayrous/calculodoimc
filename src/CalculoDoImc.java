
/**
	*Programa que calcula o IMC do paciente
	*exibe se ele esta dentro ou fora do peso
	*sendo o peso normal entre 18.5 e 24.9
	*formula do imc = peso/altura²
	*@Author Iris Campanella Cabral
	*/

import javax.swing.JOptionPane;

public class CalculoDoImc {
	public static void main(String[] args) {
		// obtendo o peso do usuario
		String peso = JOptionPane.showInputDialog(null, "Seu peso em kg: ", "Peso...", JOptionPane.QUESTION_MESSAGE)
				.replace(",", ".");
		/*
		 * converte peso em string para Double conversao estatica (exemplo: int x =
		 * Integer.parseInt();)
		 */
		double pesoEmKg = Double.parseDouble(peso);

		// obtendo a altura
		String altura = JOptionPane
				.showInputDialog(null, "Sua altura em M: ", "Altura...", JOptionPane.QUESTION_MESSAGE)
				.replace(",", ".");
		// converte a altura em string para Double
		double alturaEmM = Double.parseDouble(altura);

		// calcula o imc
		double imc = pesoEmKg / Math.pow(alturaEmM, 2);

		if (alturaEmM <= 0 || pesoEmKg <= 0) {
			JOptionPane.showMessageDialog(null, "Voc� digitou um valor inv�lido, mig�o!", "DADOS INV�LIDOS",
					JOptionPane.ERROR_MESSAGE);
			System.exit(0);

		}
		// faz a classificacao

		if (imc > 0 && imc < 18.5) {
			JOptionPane.showMessageDialog(null, "Seu imc: " + imc);
			JOptionPane.showMessageDialog(null, "Voc� est� abaixo do peso!", "Classifica��o...",
					JOptionPane.WARNING_MESSAGE);

		} else if (imc >= 18.5 && imc < 25) {
			JOptionPane.showMessageDialog(null, "Seu imc: " + imc);
			JOptionPane.showMessageDialog(null, "Voc� est� com peso normal!", "Classifica��o...",
					JOptionPane.INFORMATION_MESSAGE);

		} else if (imc >= 25 && imc < 30) {
			JOptionPane.showMessageDialog(null, "Seu imc: " + imc);
			JOptionPane.showMessageDialog(null, "Voc� est� com excesso do peso!", "Classifica��o...",
					JOptionPane.WARNING_MESSAGE);

		} else if (imc >= 30 && imc < 35) {
			JOptionPane.showMessageDialog(null, "Seu imc: " + imc);
			JOptionPane.showMessageDialog(null, "Voc� est� classificado com Obesidade classe I!", "Classifica��o...",
					JOptionPane.WARNING_MESSAGE);

		} else if (imc >= 35 && imc < 40) {
			JOptionPane.showMessageDialog(null, "Seu imc: " + imc);
			JOptionPane.showMessageDialog(null, "Voc� est� classificado com Obesidade classe II!", "Classifica��o...",
					JOptionPane.WARNING_MESSAGE);

		} else if (imc >= 40) {
			JOptionPane.showMessageDialog(null, "Seu imc: " + imc);
			JOptionPane.showMessageDialog(null, "Voc� est� classificado com Obesidade classe III!", "Classifica��o...",
					JOptionPane.WARNING_MESSAGE);

		}

	}
}
